﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] string filename = "\\world.txt";
    [HideInInspector] public GameObject player;

    public static GameManager GetInstance() { return _instance; }
    private static GameManager _instance;

    void Awake()
    {
        //singleton одиночка
        if (!_instance) _instance = this;

        transform.GetChild(1).GetChild(1)
            .GetComponent<Button>()
            .interactable = 
            File.Exists(Application.dataPath + filename);
        DontDestroyOnLoad(gameObject);
    }
    public void Create()
    {
        SceneManager.LoadScene(1);
    }
    public void Exit()
    {
        Application.Quit();
    }
    public void Save()
    {

    }
    public void Load()
    {

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            bool current = GetComponent<Canvas>().enabled;
            GetComponent<Canvas>().enabled = !current;
            
            player.GetComponent<UnityStandardAssets
                .Characters.FirstPerson
                .FirstPersonController>().enabled =
                current;
                
            if (!current) Cursor.lockState =
                    CursorLockMode.None;
            Cursor.visible = !current;
            Time.timeScale = current ? 1 : 0;
        }
    }
    void OnLevelWasLoaded(int level)
    {
        if (level > 0)
            GetComponent<Canvas>().enabled = false;
    }
}
