﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum States
{
    Idle,
    Walk,
    Run,
    Attack,
    Damage,
    KnockBack,
    Death
}

public class Enemy : MonoBehaviour
{
    Animator anim;
    PlayerController pc;
    [SerializeField] float speed;
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        if (anim.GetInteger("state") >= (int)States.Attack
            && anim.GetCurrentAnimatorStateInfo(0)
            .normalizedTime < 1
            ) return;

        if (pc)
        {
            // видим
            transform.LookAt(pc.transform);

            if (Vector3.Magnitude
                (pc.transform.position - transform.position)
                < 3f)
                anim.SetInteger("state", (int)States.Attack);
            else
            {
                anim.SetInteger("state", (int)States.Walk);
                transform.Translate(speed * Vector3.forward);
            }
        }
        else
            anim.SetInteger("state", (int)States.Idle);


        //anim.SetInteger("state", (int)States.Death);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerController>())
            pc = other.GetComponent<PlayerController>();
    }
    void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<PlayerController>())
            pc = null;
    }
    public void Attack()
    {
        pc.GetComponent<HP>().GetDamage(10);
    }
}
