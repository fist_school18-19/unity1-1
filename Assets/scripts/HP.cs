﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HP : MonoBehaviour
{
    [SerializeField] int health;
    [SerializeField] GameObject destroyed;
    int startHealth;
    
    // ui components:
    [Header("Visual components")]
    [SerializeField] Image healthBar;
    [SerializeField] Text healthText;

    Animator anim;
    void Start()
    {
        anim = GetComponent<Animator>();
        startHealth = health;
        if (healthText) healthText.text = health.ToString();
    }

    public void GetDamage(int damage)
    {
        if (anim)
            anim.SetInteger("state", (int)States.Damage);

        health -= damage;

        if (healthText) healthText.text = health.ToString();
        if (healthBar)
            healthBar.fillAmount = (1f * health / startHealth);

        if (health <= 0)
        {
            if (anim)
            {
                anim.SetInteger("state", (int)States.Death);
                Destroy(GetComponent<Enemy>());
            }
            else
            {
                if (destroyed) GameObject.Instantiate(
                    destroyed,
                    transform.position,
                    transform.rotation,
                    transform.parent
                    );
                Destroy(gameObject);
            }
            foreach(Collider c in GetComponents<Collider>())
            {
                Destroy(c);
            }

            Destroy(this);
        }
    }
}
